pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

		
contract SmartCourses{
    
    struct Lecture{
        string name;
        uint16 maxStudent;
        uint8 dailyCourseFee;
        uint8 courseDays;
        uint totalFee;
        address lecAddress;
        }

    struct Institution{
        address InstAddress;
    	string name;
    }

    struct Lecturer{
        address lecAddress;
        string name;
        bytes32 lecturerkey;
    }
    
    struct Student{
        string name;
        address StuAddress;
        uint32 StuID;
        bytes32 hashOfKey;
        bool isValid;
    }
    

    Student[] students;
    uint16 studentAmount = 0; //Total number of students
    uint8 lectureAmount = 0;
    Lecturer lecturer;  //Create a lecturer instance from Lecturer struct
    Lecture[] lectures;
        
    function setLecturer() public{
        lecturer.lecAddress = 0x123;
        lecturer.name = "Engur";
        lecturer.lecturerkey = keccak256("is_satoshi");
    }

    bytes32[][] dailyKeys;
    /* Daily keys are determined by lecturer for each day of course. A student is validated for course when its
all daily keys are determined. Each day of course, lecturer gives the code of this day to the student. Student enters this
key to attendance function and proves the attendance. We assume a ideal lecturer. */


    mapping(address=>Student) studentAddresses;
    mapping(string=> Lecture) lectureMap;


    function createLecture(string _lecturerkey, string _name, uint16 _maxStu, uint8 _fee, uint8 _days) public{
        
	//this function creates new lectures. Only lecturers can call it.
	   // require(msg.sender == lecturer.lecAddress,
	     //   'You do not have permission to create course.');
	    
	   // require(lecturer.lecturerkey== keccak256(abi.encodePacked(_lecturerkey)),
	     //   'You do not have permission to create course.');
        Lecture storage newLecture;
        newLecture.name = _name;
        newLecture.maxStudent = _maxStu;
        newLecture.dailyCourseFee = _fee;
        newLecture.courseDays = _days;
        newLecture.lecAddress = lecturer.lecAddress;
        lectures[lectureAmount] = newLecture;
        lectureAmount++;

    }
    
    
    function LectureRegistration(string _name, string _key, string _lectureName) payable public {
        require(msg.value >= lectureMap[_lectureName].totalFee, "Amount is not feasible.");
        students[studentAmount].name = _name;
        students[studentAmount].hashOfKey = keccak256(abi.encodePacked(_key));
        students[studentAmount].StuAddress = msg.sender;
        students[studentAmount].isValid = false;
        studentAmount++;
    }



    function validateStudent (uint _studentId, string[] _dailyKeyArray) public{
        require(students[i].isValid==false, "Ogrenci onceden kayit edilmis.");
        for (uint i=0; i<_dailyKeyArray.length;i++){
            dailyKeys[_studentId][i]=keccak256(abi.encodePacked(_dailyKeyArray[i]));
        }
        students[i].isValid = true;
    }
    
    function attendance (uint _id, string _studentKey, string _dailyKey, uint _day, string _lecName) public{
        require( students[_id].hashOfKey == sha256(abi.encodePacked(_studentKey)), "Authentication failured." );
        require( dailyKeys[_id][_day] == sha256(abi.encodePacked(_dailyKey)), "Authentication failured." );
        dailyKeys[_id][_day] == 0;
        msg.sender.transfer( lectureMap[_lecName].dailyCourseFee /2 );
        lectureMap[_lecName].lecAddress.transfer( lectureMap[_lecName].dailyCourseFee/2 );
    }
}
